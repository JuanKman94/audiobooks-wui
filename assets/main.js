const HISTORY_KEY = "AUDIO_BOOKS_HISTORY";

function mimickAudioEvent(book) {
  return {
    preventDefault: function() {},
    stopPropagation: function() {},
    target: {
      dataset: {
        book: book
      },
      textContent: book
    }
  };
}

function basename(str) {
  const arr = str.split("/");

  return arr[arr.length-1];
}

function setPlaylist(ev) {
  ev.preventDefault();
  ev.stopPropagation();
  const bookName = ev.target.textContent;
  const book = ev.target.dataset.book;

  window.ap.setPlaylist(audiobook2playlist(book));
  document.querySelector("#selected_book").textContent = bookName;
  document.querySelector("#synopsis").innerHTML = window.AUDIOBOOKS[bookName].synopsis;
  window.setTimeout(renderCheckboxes, 100);
}

function audiobook2playlist(bookName) {
  return window.AUDIOBOOKS[bookName]
    .files
    .map((el) => {
      return {
        artist: bookName,
        src: el,
        title: basename(el)
      };
    });
}

function renderAudiobooksList(audiobooks = {}) {
  const library = document.querySelector("#books_library");
  let a = null,
    bookName = null,
    li = null;

  for (bookName in audiobooks) {
    a = document.createElement("a");
    li = document.createElement("li");

    a.dataset.book = bookName;
    a.href = "#";
    a.textContent = bookName;
    a.addEventListener("click", setPlaylist);
    li.classList.add("audiobook-entry");

    li.append(a);
    library.append(li);
  }
}

function initPlayer() {
  window.ap = new AudioPlayer({
    controls: {
      toggle: document.querySelector("#toggle"), // play/pause button
      prev: document.querySelector("#prev"),
      next: document.querySelector("#next"),
      volume: document.querySelector("#volume"), // volume (input[type=range])
      seek: document.querySelector("#seek"), // song progress (time) (input[type=range])
      currentTime: document.querySelector("#currentTime"), // song current time, e.g., 02:34
      duration: document.querySelector("#duration"), // song duration, e.g., 04:27
      artist: document.querySelector("#artist"),
      title: document.querySelector("#title"),
      // <ul>/<ol> where to add the playlist entries (<li>)
      playlist: document.querySelector("#book_playlist"),
      volumePerc: document.querySelector("#volume_perc"),
    },
    labels: {
      playlistItem: "%T",
      pause: "||", // HTML-text used in toggle button when in pause
    },
  });

  document.querySelector("#rewind").addEventListener("click", function(ev) {
    if ((window.ap.player.currentTime - 15) > 0)
      window.ap.player.currentTime -= 15;
  });
  document.querySelector("#forward").addEventListener("click", function(ev) {
    if (window.ap.player.duration > (window.ap.player.currentTime + 15))
      window.ap.player.currentTime += 15;
  });

  const hotkeys = {
    "Space": function() { window.ap.togglePlay() },
    "ArrowUp": function() { window.ap.setVolume(window.ap.player.volume+0.025) },
    "ArrowRight": function() { window.ap.playNextSong() },
    "ArrowDown": function() { window.ap.setVolume(window.ap.player.volume-0.025) },
    "ArrowLeft": function() { window.ap.playPrevSong() },
  };

  document.addEventListener("keydown", (ev) => {
    if (hotkeys[ev.code]) {
      ev.preventDefault();
      hotkeys[ev.code]()
    }
  });
}

function getCurrentBook() {
  return document.querySelector("#selected_book").textContent;
}

function audioSrc2playlistSrc(audioSrc, bookName) {
  bookName = bookName || getCurrentBook();
  const bookFiles = window.AUDIOBOOKS[bookName].files;
  const decodedSrc = window.decodeURI(audioSrc);
  let idx = 0,
    result = null;

  for (idx = 0; idx < bookFiles.length; ++idx) {
    if (decodedSrc.includes(bookFiles[idx])) {
      result = bookFiles[idx];
      break;
    }
  }

  return result;
}

/**
 * Mark audio file as already listened to.
 *
 * @param [String] bookName playlist name
 * @param [String] fileName as found in playlist array, used for audioplayer's src
 * @param [String] action to perform in history, defaults to "add"
 */
function markFileAs(bookName, fileName, action = "add") {
  bookName = bookName || getCurrentBook();
  const history = readHistory();
  let idx = -1;
  let error = null;

  if (!bookName) error = "markFileAs: bookName required";
  if (!fileName) error = "markFileAs: fileName required";

  if (error) {
    console.error(error);
    return;
  }

  if (!history[bookName]) { history[bookName] = [] }

  idx = history[bookName].indexOf(fileName);

  switch(action) {
    case "add":
    case "complete":
      if (idx === -1) {
        history[bookName].push(fileName);
      }
      break;
    case "notcomplete":
    case "remove":
      if (idx !== -1) {
        history[bookName].splice(idx, 1);
      }
      break;
    default:
      console.warn(`markFileAs: Invalid action: '${action}'`);
      break;
  }

  updateHistory(history);
}

function checkboxChangeHandler(ev) {
  ev.stopPropagation();

  if (ev.target.checked) {
    markFileAs(getCurrentBook(), ev.target.value);
  } else {
    markFileAs(getCurrentBook(), ev.target.value, "remove");
  }
}

function renderCheckboxes() {
  const list = document.querySelector("#book_playlist");
  const bookName = getCurrentBook();
  const history = readHistory();
  let checkbox = null,
    a = null;

  if (!history[bookName]) { history[bookName] = [] }

  Array.from(list.children).forEach((li, i) => {
    a = li.querySelector("a");
    checkbox = li.querySelector("input[type=checkbox]");

    if (!checkbox) {
      checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      li.prepend(checkbox);
    }

    checkbox.value = a.dataset.src;
    if (history[bookName].indexOf(checkbox.value) !== -1) {
      checkbox.checked = true;
    }
    checkbox.addEventListener("change", checkboxChangeHandler);
  });
}

function updateHistory(newHistory) {
  window.localStorage.setItem(HISTORY_KEY, JSON.stringify(newHistory));
}

function readHistory() {
  return window.AUDIO_BOOK_HISTORY = JSON.parse(window.localStorage.getItem(HISTORY_KEY) || "{}");
}

function playlistItemAt(idx, bookName) {
  bookName = bookName || getCurrentBook();

  return window.AUDIOBOOKS[bookName].files[idx];
}

function playlistItemIndex(fileSrc, bookName) {
  bookName = bookName || getCurrentBook();

  return window.AUDIOBOOKS[bookName].files.indexOf(fileSrc);
}

function findLastMarkedFile(bookName) {
  bookName = bookName || getCurrentBook();
  const srcFiles = window.AUDIOBOOKS[bookName].files;
  const history = readHistory();
  const len = srcFiles.length;
  let i = 0;

  if (!history[bookName]) {
    console.error(`findLastMarkedFile: first time reading book '${bookName}'`);
    return;
  }

  const historyFiles = history[bookName];
  historyFiles.sort(); // just in case
  for (i = 0; i < len; ++i) {
    if (srcFiles[i] !== historyFiles[i]) {
      break;
    }
  }

  return srcFiles[i];
}

function scrollToPlaylistEntry(fileSrc) {
  document.querySelector(`[data-src="${fileSrc}"]`)?.scrollIntoView({ behavior: 'smooth' });
}

function init() {
  let defaultBook = null,
    k = null;
  console.info("Initializing audiobooks player...");
  initPlayer();
  renderAudiobooksList(window.AUDIOBOOKS);

  for (k in window.AUDIOBOOKS) {
    defaultBook = k;
    break;
  }

  if (defaultBook) {
    setPlaylist(mimickAudioEvent(defaultBook));
    renderCheckboxes();

    const lastPlayedFile = findLastMarkedFile(defaultBook);

    if (lastPlayedFile) {
      scrollToPlaylistEntry(lastPlayedFile);
      window.ap.setSong(playlistItemIndex(lastPlayedFile, defaultBook));
    }
  }

  window.ap.player.addEventListener("ended", function(ev) {
    const bookName = getCurrentBook();
    const nextFileSrc = audioSrc2playlistSrc(ev.originalTarget.src, bookName);
    // get index of **new** current file, i.e., file1 ended, audio.src points to file2
    const nextFileIdx = playlistItemIndex(nextFileSrc, bookName);

    markFileAs(bookName, playlistItemAt(nextFileIdx - 1, bookName));
    renderCheckboxes();
    scrollToPlaylistEntry(nextFileSrc);
  });
}

window.addEventListener("DOMContentLoaded", init);
